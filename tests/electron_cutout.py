"""
   VirtuaLinac.tuning_electrons
   ~~~~~~~~~~~~~~~~~~~~~~~

   Calculate dose for electrons beams, for entire simulation 

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# create_ct.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.



# Set the environment variable VL_TEST_PHSPDIR and VL_TEST_DATADIR
# to point to the directories in which the input phase space files and
# measurement data are, respectively.

import os,sys
import time
import requests
import shutil
import vl_api_helper as helper
import tuning_electrons_config

def electron_cutout(url,
                     precision=0,
                     code_version='10.2.p2',
                     upload=False,
                     filename='electron_cutout',
                     beams=None):
    """ Calculate dose to water for electronbeams, starting from e beam"""
    ### starting from myvarian phase space file

    if beams is None:
        #beams = ['6E', '9E', '12E', '15E', '16E', '18E', '20E', '22E']
        beams = ['12E']
    
    #config = tuning_electrons_config.config()
    params = tuning_electrons_config.params()
    if precision == 0:
        params['incident_particles'] = 10000000
    else:
        params['incident_particles'] = 30000000

    directory = filename
    #if os.path.exists(directory):
    #    directory = directory + '_' + str(int(time.time()))
    if not os.path.isdir(directory):
        os.mkdir(directory)

    for beam in beams:

        ## incident beam

        filenamebeam    = filename + '_' + beam

        #data_dir = os.path.join(config['data_dir'], beam)
        #pdd_msmt = config['pdd'][beam]
        #prf_msmt = config['prf'][beam]
        #prf_air_msmt = config['airprf'][beam]

        params['range_cut'] = 10
        params['energy'] = 13.18
        params['energy_spread'] = 0.7
        params['spot_size_x'] = 0.7
        params['spot_size_y'] = 0.7
        params['foil1'] = '2'
        params['foil2'] = '2'
        params['ic_metal_thickness_factor'] = 0.64
        params['foil1_thickness_factor'] = 1.02
        params['applicator']='1'
        #params['cutout'] = 'True'
        #params['cutout_vertices']='[.905,0.957],[.905,-0.957],[-0.905,-0.957],[-0.905,.957]'
        #params['cutout_thickness']=13.9
        #params['cutout_bevel_factor']=0.99
        #params['cutout_material']='cerrotru'
        params['jaw_position_x1']=-5.5
        params['jaw_position_x2']= 5.5
        params['jaw_position_y1']=-5.5
        params['jaw_position_y2']= 5.5

        print '--------------------------------------'
        print 'This is ' + filenamebeam + ' (dose)'
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(url+'/delete/'+filenamebeam + '.dose')

        if 0: #upload:
            #upload measurement files
            print 'Uploading measurement files.'
            upload_url = url + '/upload'
            files = {'file': open(os.path.join(data_dir, pdd_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_air_msmt), 'rb')}
            r = requests.post(upload_url, files=files)

        #print options
        params['filename']     = filenamebeam
        params['phantom_bool'] = 'True'
        params['phantom_voxels_z']   = 50 #phantom_z
        params['phantom_voxels_x']   = 40
        params['phantom_voxels_y']   = 40
        params['phantom_size_z']     = 100
        params['phantom_size_x']     = 100
        params['phantom_size_y']     = 100
        params['phantom_position_z'] = -5
            ## center of voxel, convert to cm
        #params['phsp_record']  =  'True'
        #params['phsp_position'] = 50

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(url, data=params)
        helper._wait_for_jobs(url)

        average = 2
        if precision == 0:
            average = 5

        # get output file and macro
        ofn = filenamebeam + '.output'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ofn = filenamebeam + '.mac'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ofn = filenamebeam + '.dictionary'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ofn = filenamebeam + '.dose'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)


