"""
   VirtuaLinac.tuning_photons
   ~~~~~~~~~~~~~~~~~~~~~~~

   Simulate tx head starting from e- beam, calculate doses
   Configuration data for myvarian_photons tests

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# tuning_photons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Set the environment variable VL_TEST_PHSPDIR and VL_TEST_DATADIR
# to point to the directories in which the input phase space files and
# measurement data are, respectively.

import os,sys
import time
import requests
import shutil
import vl_api_helper as helper
import tuning_photons_config

def tuning_photons(url,
                   precision=0,
                   upload=True,
                   filename='tuning_photons',
                   beams=None):
    """ Calculate dose to water for photon beams, starting from e beam"""

    if beams is None:
        beams = ['2.5X', '4X', '6X', '6FFF', '8X', '10X', '10FFF', '15X']
    
    config  = tuning_photons_config.config()
    params  = tuning_photons_config.params()
    if precision == 0:
        params['incident_particles'] = 100000
    elif precision == 1:
        params['incident_particles'] = 1000000
    else:
        params['incident_particles'] = 20000000

    directory = filename
    if not os.path.isdir(directory):
        os.mkdir(directory)

    for beam in beams:
        ## incident beam
        filenamebeam    = filename + '_' + beam

        data_dir = os.path.join(config['data_dir'], beam)
        pdd_msmt = config['pdd'][beam]
        prf_msmt = config['prf'][beam]

        params['energy'] = config['energy'][beam]
        params['energy_spread'] = config['energy_spread'][beam]
        params['spot_size_x'] = config['spot_size_x'][beam]
        params['spot_size_y'] = config['spot_size_y'][beam]
        params['beam_divergence_x'] = config['beam_divergence_x']
        params['beam_divergence_y'] = config['beam_divergence_y']
        params['target'] = config['target'][beam]
        params['flattening_filter'] = config['flattening_filter'][beam]

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(url+'/delete/'+filenamebeam + '.dose')

        if upload:
            #upload measurement files
            print 'Uploading measurement files.'
            upload_url = url + '/upload'
            files = {'file': open(os.path.join(data_dir, pdd_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_msmt), 'rb')}
            r = requests.post(upload_url, files=files)

        params['filename']     = filenamebeam

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(url, data=params)
        helper._wait_for_jobs(url)

        average = 2
        if precision < 2:
            average = 5

        # get output file and macro
        ofn = filenamebeam + '.output'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ofn = filenamebeam + '.mac'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        # plot dose distributions
        plt_url = url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : config['voxel_z'][beam],
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.X.png', 
                params=plt_params, directory=directory)

