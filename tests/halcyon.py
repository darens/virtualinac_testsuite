"""
   VirtuaLinac.tuning_photons
   ~~~~~~~~~~~~~~~~~~~~~~~

   Simulate tx head starting from e- beam, calculate doses
   Configuration data for myvarian_photons tests

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# tuning_photons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Set the environment variable VL_TEST_PHSPDIR and VL_TEST_DATADIR
# to point to the directories in which the input phase space files and
# measurement data are, respectively.

import os,sys
import time
import requests
import shutil
import getpass
import vl_api_helper as helper

class Halcyon(object):

    def __init__(self,
                 url,
                 filename='testing',
                 precision=0):
        self.url = url
        self.up_url = url + '/upload'
        self.precision = precision
        self.directory = filename
        self.data_dir = os.path.join('/','home', 'daren', 'montecarlo',
                        'TrueBeamData','halcyon')
        #self.input_dir = os.path.join('/', 'home', 'daren', 'montecarlo', 'virtualinac', 'bitbucket', 'api', 'tests', 'inputs')
        #self.input_dir = os.path.join('/', 'home', 'daren', 'g4', 'truebeam', 'build', 'h')

        self.inputdir = os.path.join('/', 'home', getpass.getuser(), 'montecarlo',
                    'virtualinac', 'bitbucket', 'api', 'tests', 'inputs')

        if not os.path.isdir(self.directory):
            os.mkdir(self.directory)

        self.params = {}
        self.params['platform'] = 'Halcyon'
        self.params['physics_list'] = 'QGSP_BIC_EMZ'
        self.params['beam_type']    = 0
        self.params['range_cut'] = 10
        self.params['phantom_bool'] = 'True'
        self.params['phantom_size_x'] = 400
        self.params['phantom_size_y'] = 400
        self.params['phantom_size_z'] = 400
        self.params['phantom_voxels_x'] = 100
        self.params['phantom_voxels_y'] = 100
        self.params['phantom_voxels_z'] = 100
        self.params['phantom_position_x'] = 0
        self.params['phantom_position_y'] = 0
        self.params['phantom_position_z'] = -10  # SSD 90
        self.params['brem_splitting'] = 200
        self.params['splitting_factor'] = 20
        self.params['trajectory_bool'] = 'True'
        self.params['trajectory_beamlets_mu'] = 1
        self.params['trajectory_particles_mu'] = 5000000
        if self.precision > 0:
            self.params['trajectory_particles_mu'] = 10000000
        ## incident beam
        self.params['energy'] = 5.95
        self.params['energy_spread'] = 0.18
        self.params['spot_size_x'] = 1.0
        self.params['spot_size_y'] = 1.0
        self.params['beam_divergence_x'] = 0
        self.params['beam_divergence_y'] = 0

    def generate_phsp(self,
                     filename='params',
                     energy=None):
        """ Generate phsp files """

        xmlfile = 'field_28x28.xml'
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
       
        filename = self.directory+'_28x28'

        params = self.params.copy()
        
        if energy is not None:
            params['energy'] = energy
            filename     = filename + '_' + str(energy)

        params['filename']     = filename


        print '--------------------------------------'
        print 'This is ' + filename
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filename + '.phsp')
        params['phantom_bool'] = None
        params['phsp_record'] = True
        params['phsp_position'] = '693'
        params['random_number_seed'] = 123456

        params['trajectory_file'] = xmlfile

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filename,phsp=True)



    def params_28x28(self,
                     filename='params',
                     energy=None):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_28x28.xml'
        pdd_msmt = 'pdd_28.dat'
        prf_msmt_dmax = 'xplane_28_1p3.dat'
        prf_msmt_5    = 'xplane_28_5p0.dat'
        prf_msmt_10   = 'xplane_28_10p0.dat'
        prf_msmt_20   = 'xplane_28_20p0.dat'
        prf_msmt_30   = 'xplane_28_30p0.dat'
        
        filename = self.directory+'_28x28'

        params = self.params.copy()

        
        if energy is not None:
            params['energy'] = energy
            filename     = filename + '_' + str(energy)

        params['filename']     = filename



        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)


        print '--------------------------------------'
        print 'This is ' + filename
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filename + '.dose')

        params['phantom_size_x'] = 480
        params['phantom_size_y'] = 480
        params['phantom_voxels_x'] = 120
        params['phantom_voxels_y'] = 120

        params['trajectory_file'] = xmlfile

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filename)

        average = 3

        # plot dose distributions
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filename + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filename + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filename + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filename + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filename + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filename + '.X_30.png', 
                params=plt_params, directory=self.directory)

    def params_20x20(self,
                     filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_20x20.xml'
        pdd_msmt = 'pdd_20.dat'
        prf_msmt_dmax = 'xplane_20_1p3.dat'
        prf_msmt_5    = 'xplane_20_5p0.dat'
        prf_msmt_10   = 'xplane_20_10p0.dat'
        prf_msmt_20   = 'xplane_20_20p0.dat'
        prf_msmt_30   = 'xplane_20_30p0.dat'
        
        filename = self.directory+'_20x20'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 3

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)



    def params_10x10(self,
                     filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_10x10.xml'
        pdd_msmt = 'pdd_10.dat'
        prf_msmt_dmax = 'xplane_10_1p3.dat'
        prf_msmt_5    = 'xplane_10_5p0.dat'
        prf_msmt_10   = 'xplane_10_10p0.dat'
        prf_msmt_20   = 'xplane_10_20p0.dat'
        prf_msmt_30   = 'xplane_10_30p0.dat'
 
        filename = self.directory+'_10x10'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 200
        params['phantom_size_y'] = 200
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 50
        params['phantom_voxels_y'] = 50
        params['phantom_voxels_z'] = 100

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 3

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)


    def params_8x8(self,
                   filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_8x8.xml'
        pdd_msmt = 'pdd_8.dat'
        prf_msmt_dmax = 'xplane_8_1p3.dat'
        prf_msmt_5    = 'xplane_8_5p0.dat'
        prf_msmt_10   = 'xplane_8_10p0.dat'
        prf_msmt_20   = 'xplane_8_20p0.dat'
        prf_msmt_30   = 'xplane_8_30p0.dat'
 
        filename = self.directory+'_8x8'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 200
        params['phantom_size_y'] = 200
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 100
        params['phantom_voxels_y'] = 100
        params['phantom_voxels_z'] = 100

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 2

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)


    def params_6x6(self,
                   filename='params',
                   energy=None):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_6x6.xml'
        pdd_msmt = 'pdd_6.dat'
        prf_msmt_dmax = 'xplane_6_1p3.dat'
        prf_msmt_5    = 'xplane_6_5p0.dat'
        prf_msmt_10   = 'xplane_6_10p0.dat'
        prf_msmt_20   = 'xplane_6_20p0.dat'
        prf_msmt_30   = 'xplane_6_30p0.dat'
        
        filename = self.directory+'_6x6'

        filenamebeam    = filename

        params = self.params.copy()

        if energy is not None:
            params['energy'] = energy
            params['filename']     = filenamebeam + '_' + str(energy)
        
        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)


        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')


        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 200
        params['phantom_size_y'] = 200
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 200
        params['phantom_voxels_y'] = 200
        params['phantom_voxels_z'] = 100



        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 1

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)


    def params_4x4(self,
                   filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_4x4.xml'
        pdd_msmt = 'pdd_4.dat'
        prf_msmt = 'xplane_4_1p3.dat'
        prf_msmt_dmax = 'xplane_4_1p3.dat'
        prf_msmt_5    = 'xplane_4_5p0.dat'
        prf_msmt_10   = 'xplane_4_10p0.dat'
        prf_msmt_20   = 'xplane_4_20p0.dat'
        prf_msmt_30   = 'xplane_4_30p0.dat'
 
        filename = self.directory+'_4x4'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 100
        params['phantom_size_y'] = 100
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 200
        params['phantom_voxels_y'] = 200
        params['phantom_voxels_z'] = 100

        self.params['trajectory_particles_mu'] *= 4
        
        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 1

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)


    def params_2x2(self,
                   filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_2x2.xml'
        pdd_msmt = 'pdd_2.dat'
        prf_msmt_dmax = 'xplane_2_1p3.dat'
        prf_msmt_5    = 'xplane_2_5p0.dat'
        prf_msmt_10   = 'xplane_2_10p0.dat'
        prf_msmt_20   = 'xplane_2_20p0.dat'
        prf_msmt_30   = 'xplane_2_30p0.dat'
 
        filename = self.directory+'_2x2'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.data_dir, pdd_msmt), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_dmax), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_5), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_10), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_20), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.data_dir, prf_msmt_30), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(self.url+'/delete/'+filenamebeam + '.dose')

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 100
        params['phantom_size_y'] = 100
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 100
        params['phantom_voxels_y'] = 100
        params['phantom_voxels_z'] = 100

        self.params['trajectory_particles_mu'] *= 4
        
        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam)

        average = 1

        # plot dose distributions
        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '3',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_dmax
        }
        helper.get_file(plt_url, filenamebeam + '.X_dmax.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '12',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_5
        }
        helper.get_file(plt_url, filenamebeam + '.X_5.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '24',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_10
        }
        helper.get_file(plt_url, filenamebeam + '.X_10.png', 
                params=plt_params, directory=self.directory)
        
        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '49',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_20
        }
        helper.get_file(plt_url, filenamebeam + '.X_20.png', 
                params=plt_params, directory=self.directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : '74',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt_30
        }
        helper.get_file(plt_url, filenamebeam + '.X_30.png', 
                params=plt_params, directory=self.directory)



    def distal_closed(self,
                      filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_distal_closed.xml'
 
        filename = self.directory+'_dist'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        self.delete_all(filenamebeam)

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 300
        params['phantom_size_y'] = 300
        params['phantom_size_z'] = 50
        params['phantom_voxels_x'] = 300
        params['phantom_voxels_y'] = 300
        params['phantom_voxels_z'] = 25
        params['phantom_position_z'] = -2.5
        params['trajectory_particles_mu'] *= 4

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam, phsp=False)

        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'y',
            'average'   : 1,
            'zplot'     : 7
        }
        helper.get_file(plt_url, filenamebeam + '.Y.png', 
                params=plt_params, directory=self.directory)


        #plt_url = self.url + '/figphsp/' + filename + '_field.phsp'
        #plt_params = {
        #    'colormap'  : 'True',
        #    'particle'  : 1,
        #    'yaxis'     : 'y',
        #    'xaxis'     : 'x',
        #    'factor'    : 'en',
        #    'xmin'      : '-20.0',
        #    'xmax'      : '20.0',
        #    'ymin'      : '-20.0',
        #    'ymax'      : '20.0',
        #    'ynumber_bins' : 400,
        #    'xnumber_bins' : 400
        #}
        #helper.get_file(plt_url, filename + '.phsp.2d.png',
        #            params=plt_params, directory=self.directory)

    def proximal_closed(self,
                      filename='params'):
        """ Calculate dose to water for photon beams, starting from e beam"""

        xmlfile = 'field_proximal_closed.xml'
 
        filename = self.directory+'_prox'

        #upload files
        print 'Uploading measurement files.'
        files = {'file': open(os.path.join(self.inputdir, xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        filenamebeam    = filename

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        self.delete_all(filenamebeam)

        params = self.params.copy()

        params['filename']     = filenamebeam
        params['trajectory_file'] = xmlfile

        params['phantom_size_x'] = 300
        params['phantom_size_y'] = 300
        params['phantom_size_z'] = 50
        params['phantom_voxels_x'] = 300
        params['phantom_voxels_y'] = 300
        params['phantom_voxels_z'] = 25
        params['trajectory_particles_mu'] *= 4

        #params['phantom_bool'] = None
        #params['phsp_record'] = 'True'
        #params['phsp_position'] = 0

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filenamebeam, phsp=False)

        plt_url = self.url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'y',
            'average'   : 1,
            'zplot'     : 7
        }
        helper.get_file(plt_url, filenamebeam + '.Y.png', 
                params=plt_params, directory=self.directory)


    def prostate(self):
        """ xml trajectory for ct file """
        ## prostate plan
        filename    = 'halcyon_prostate'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        phantomfile = 'halcyon_prostate.phantom'
        xmlfile     = 'halcyon_prostate.xml'
        #upload phantom and xml files
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)
        files = {'file': open(os.path.join(self.inputdir,phantomfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['platform'] = 'Halcyon'
        params['physics_list'] = 'QGSP_BIC'
        params['range_cut']  = 1000.0
        params['beam_type']    = 0
        params['energy']       = 5.95
        params['energy_sigma'] = 0.18
        params['flattening_filter'] = "open port"
        params['phantom_bool'] = 'True'
        params['phantom_file_bool'] = 'True'
        params['phantom_file_name'] = phantomfile
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 2
        params['trajectory_particles_mu'] = 10000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        params['brem_splitting'] = 500
        params['splitting_factor'] = 50
        params['fast_simulation'] = 'True'
        #params['phantom_dose_average'] = 5
        #params['phantom_dose_average_type'] = 'triangle'
        params['kill_primColl'] = 10000
        params['kill_mlc'] = 1000


        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filename, phsp=False)
        
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True',
            'voxel_x'   : '129',
            'voxel_y'   : '86',
            'voxel_z'   : '128'
        }
        helper.get_file(plt_url, filename + '.2d.x.png',
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png',
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png',
                    params=plt_params, directory=self.directory)


    def penumbra(self):
        """ high resolution phantom to measure penumbra """
        filename    = 'halcyon_penumbra'
        print '--------------------------------------'
        print 'This is test', filename
        print '--------------------------------------'
        xmlfile     = 'field_10x10.xml'
        #upload xml files
        files = {'file': open(os.path.join(self.inputdir,xmlfile), 'rb')}
        r = requests.post(self.up_url, files=files)

        params = {}
        params['code_version'] = 0
        params['platform'] = 'Halcyon'
        params['physics_list'] = 'QGSP_BIC_EMZ'
        params['range_cut']  = 10.0
        params['beam_type']    = 0
        params['energy']       = 5.95
        params['energy_sigma'] = 0.18
        params['spot_size_x'] = 1.0
        params['spot_size_y'] = 1.0
        params['phantom_bool'] = 'True'
        params['trajectory_bool'] = 'True'
        params['trajectory_file'] = xmlfile
        params['trajectory_beamlets_mu'] = 1
        params['trajectory_particles_mu'] = 10000000
        if self.precision == 0:
            params['trajectory_particles_mu'] = 1000
        params['brem_splitting'] = 100
        params['splitting_factor'] = 10
        params['fast_simulation'] = None
        #params['phantom_dose_average'] = 5
        #params['phantom_dose_average_type'] = 'triangle'
        params['phantom_bool'] = 'True'
        params['phantom_size_x'] = 500
        params['phantom_size_y'] = 500
        params['phantom_size_z'] = 400
        params['phantom_voxels_x'] = 500
        params['phantom_voxels_y'] = 500
        params['phantom_voxels_z'] = 200
        params['phantom_position_x'] = 0
        params['phantom_position_y'] = 0
        params['phantom_position_z'] = -10  # SSD 90

        params['filename'] = filename

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(self.url, data=params)
        helper._wait_for_jobs(self.url)

        self.get_results(filename, phsp=False)
        
        plt_url = self.url + '/fig/' + filename + '.dose'
        plt_params = {
            'direction' : 'x',
            'colormap'  : 'True',
            'voxel_x'   : '129',
            'voxel_y'   : '86',
            'voxel_z'   : '128'
        }
        helper.get_file(plt_url, filename + '.2d.x.png',
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'y'
        helper.get_file(plt_url, filename + '.2d.y.png',
                    params=plt_params, directory=self.directory)

        plt_params['direction'] = 'z'
        helper.get_file(plt_url, filename + '.2d.z.png',
                    params=plt_params, directory=self.directory)







    def all_params(self):
        ##self.params_28x28(filename=self.directory+'_6p0_',energy=6.0)
        ##self.params_28x28(filename=self.directory+'_5p95_',energy=5.95)
        self.params_28x28()
        self.params_20x20()
        self.params_10x10()
        self.params_8x8()
        self.params_6x6()
        self.params_20x20()
        self.params_4x4()
        self.params_2x2()
        self.prostate()
        self.penumbra()
        self.generate_phsp()
        self.distal_closed()
        self.proximal_closed()

    def get_results(self, filename, phsp=False):
        # get output file and macro
        ofn = filename + '.output'
        helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)

        ofn = filename + '.mac'
        helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)

        ofn = filename + '.dose'
        helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)

        ofn = filename + '.dictionary'
        helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)

        if phsp:
            ofn = filename + '_field.phsp'
            helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)
            ofn = filename + '_field.header'
            helper.get_file(self.url + '/vl_files/' + ofn, ofn, directory=self.directory)

    def delete_all(self, filename):
        ## delete existing files
        resp = requests.get(self.url + '/delete/' + filename + '_field.phsp')
        resp = requests.get(self.url + '/delete/' + filename + '_field.header')
        resp = requests.get(self.url + '/delete/' + filename + '.dose')
        resp = requests.get(self.url + '/delete/' + filename + '.output')

