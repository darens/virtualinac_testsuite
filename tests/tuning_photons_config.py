"""
   VirtuaLinac.tuning_photons_config
   ~~~~~~~~~~~~~~~~~~~~~~~

   Configuration data for tuning_photons tests

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# tuning_photons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os

def config():

    datadir = os.getenv('VL_TEST_DATADIR')
    if not datadir:
        print 'Set the environment variable VL_TEST_DATADIR to the directory'
        print 'where the measurement data is.'
        exit()

    config = {
        'energy' : {
            '2.5X'  :  2.4,
            '4X'    :  4.5,
            '6X'    :  6.18,
            '6FFF'  :  5.90,
            '8X'    :  8.74,
            '10X'   : 10.7,
            '10FFF' : 10.2, 
            '15X'   : 13.5
        },

        'energy_spread' : {
            '2.5X'  : '0.02',
            '4X'    : 0.035,
            '6X'    : 0.053,
            '6FFF'  : 0.051,
            '8X'    : 0.074,
            '10X'   : 0.0909,
            '10FFF' : 0.0866,
            '15X'   : 0.115
        },

        'spot_size_x' : {
            '2.5X'  : 1.0,
            '4X'    : 0.7346,
            '6X'    : 0.6866,
            '6FFF'  : 0.6645,
            '8X'    : 0.8460,
            '10X'   : 0.8345,
            '10FFF' : 0.8118, 
            '15X'   : 0.6415
        },

        'spot_size_y' : {
            '2.5X'  : 1.0,
            '4X'    : 0.7986,
            '6X'    : 0.7615,
            '6FFF'  : 0.7274,
            '8X'    : 0.9292,
            '10X'   : 0.8710,
            '10FFF' : 0.8001, 
            '15X'   : 0.5768
        },

        'beam_divergence_x' : 0.0573, 
        'beam_divergence_y' : 0.0573,

        'target' : {
            '2.5X'  : 3,
            '4X'    : 0,
            '6X'    : 0,
            '6FFF'  : 0,
            '8X'    : 1,
            '10X'   : 1,
            '10FFF' : 2,
            '15X'   : 2
        },

        'flattening_filter' : {
            '2.5X'  : 7,
            '4X'    : 0,
            '6X'    : 1,
            '6FFF'  : 7,
            '8X'    : 2,
            '10X'   : 3,
            '10FFF' : 7,
            '15X'   : 4
        },

        'data_dir' : datadir,

        'pdd' : {
            '2.5X'  : 'imaging_z_pdd.dat',
            '4X'    : '4X_Z_40x40.dat',
            '6X'    : '6X_Z_40x40.dat',
            '6FFF'  : '6FFF_Z_40x40.dat',
            '8X'    : '8X_Z_40x40.dat',
            '10X'   : '10X_Z_40x40.dat',
            '10FFF' : '10FFF_Z_40x40.dat',
            '15X'   : '15X_Z_40x40.dat'
        },

        'prf' : {
            '2.5X'  : 'imaging_y_0p5.dat',
            '4X'    : '4X_X_1p2_40x40.dat',
            '6X'    : '6X_X_1p5_40x40.dat',
            '6FFF'  : '6FFF_X_1p5_40x40.dat',
            '8X'    : '8X_X_2p0_40x40.dat',
            '10X'   : '10X_X_2p4_40x40.dat',
            '10FFF' : '10FFF_X_2p4_40x40.dat',
            '15X'   : '15X_X_3p0_40x40.dat'
        },

        'voxel_z' : {
            '2.5X'  : 1,
            '4X'    : 3, #6,
            '6X'    : 4, #7,
            '6FFF'  : 4, #7,
            '8X'    : 5, #10,
            '10X'   : 6, #12,
            '10FFF' : 6, #12,
            '15X'   : 8  #15 
        },
    }

    return config


def params():
    params = {}
    params['code_version'] = 0
    params['platform'] = 'TrueBeam'
    params['physics_list'] = 'QGSP_BIC_EMZ'
    params['beam_type']    = 0
    params['range_cut'] = 10
    params['jaw_position_y1'] = -15  ## FIXME different for imaging, tx
    params['jaw_position_y2'] =  15
    params['jaw_position_x1'] = -15
    params['jaw_position_x2'] =  15
    params['phantom_bool'] = 'True'
    params['phantom_size_x'] = 500
    params['phantom_size_y'] = 500
    params['phantom_size_z'] = 400
    params['phantom_voxels_x'] = 125
    params['phantom_voxels_y'] = 125
    params['phantom_voxels_z'] = 200
    params['phantom_position_x'] = 0
    params['phantom_position_y'] = 0
    params['phantom_position_z'] = -20
    params['brem_splitting'] = 100
    params['splitting_factor'] = 10

    return params
