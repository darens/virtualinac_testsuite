"""
   VirtuaLinac.myvarian_electrons
   ~~~~~~~~~~~~~~~~~~~~~~~

   Calculate dose distributions starting from electron phase space files

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# myvarian_electrons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Set the environment variable VL_TEST_PHSPDIR and VL_TEST_DATADIR
# to point to the directories in which the input phase space files and
# measurement data are, respectively.
#

import os,sys
import time
import requests
import shutil
import vl_api_helper as helper
import myvarian_electrons_config

def myvarian_electrons(url,
                       precision=0,
                       code_version='10.2.p1',
                       upload=True,
                       phsp_index=0,                       
                       filename='myvarian_electrons',
                       beams=None):
    """ Calculate dose to water for electronbeams, starting from phsp """
    ### starting from myvarian phase space file

    if beams is None:
        beams = ['6E', '9E', '12E', '15E', '16E', '18E', '20E', '22E']
    
    config  = myvarian_electrons_config.config(phsp_index)
    params = myvarian_electrons_config.params()
    if precision == 0:
        params['incident_particles'] = 1000000

    directory = filename
    #if os.path.exists(directory):
    #    directory = directory + '_' + str(int(time.time()))
    if not os.path.isdir(directory):
        os.mkdir(directory)

    for beam in beams:

        source_phsp = config['source_phsp'][beam]
        filenamebeam    = filename + '_' + beam
        data_dir = os.path.join(config['data_dir'], beam)
        phsp_dir = os.path.join(config['phsp_dir'], beam)
        pdd_msmt = config['pdd'][beam]
        prf_msmt = config['prf'][beam]
        prf_air_msmt = config['airprf'][beam]


        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
        ## delete existing files
        resp = requests.get(url+'/delete/'+filenamebeam + '.dose')
        resp = requests.get(url+'/delete/'+filenamebeam + '_field.phsp')
        resp = requests.get(url+'/delete/'+filenamebeam + '_field.header')

        if upload:
            #upload measurement files
            print 'Uploading measurement files.'
            upload_url = url + '/upload'
            files = {'file': open(os.path.join(data_dir, pdd_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_air_msmt), 'rb')}
            r = requests.post(upload_url, files=files)

            ## upload phsp files
            print 'Uploading phase space files.'
            files = {'file': open(os.path.join(phsp_dir, source_phsp), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(
                    phsp_dir, source_phsp.split('.')[0] + '.IAEAheader'), 'rb')}
            r = requests.post(upload_url, files=files)

        #print options
        params['filename']     = filenamebeam
        params['phsp_input_file'] = source_phsp
        params['phantom_bool'] = 'True'
        phantom_z = config['phantom_z'][beam]
        params['phantom_voxels_z']   = phantom_z
        params['phantom_size_z']     = phantom_z * 2  ## 2 mm per voxel
        params['phantom_position_z'] = -phantom_z / 10 
            ## center of voxel, convert to cm

        params['phsp_record']  = 'True'
        params['phsp_position'] = 50
        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(url, data=params)
        helper._wait_for_jobs(url)

        average = 2
        if precision == 0:
            average = 5

        # get output file and macro
        ofn = filenamebeam + '.output'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ofn = filenamebeam + '.mac'
        helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        # plot dose distributions
        plt_url = url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', 
                params=plt_params, directory=directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : config['voxel_z'][beam],
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.X.png', 
                params=plt_params, directory=directory)

        # plot phase spaces
        plt_url = url + '/figphsp/' + filenamebeam + '_field.phsp'
        plt_params = {
            'particle'  : 2,
            'abscissa'  : 'x',
            'fluence'   : 'True',
            'xmin'      : '-30.0',
            'xmax'      : 30.0,
            'xnumber_bins' : 120,
            'condition' : '(u**2 + v**2 < 0.9999) and abs(y)<2',
            'compare'   : 'True',
            'msmt_file' : prf_air_msmt
        }

        helper.get_file(plt_url, filenamebeam + '.phsp.X.png',
                params=plt_params, directory=directory)

        ########### phsp ##################
        #### VL currently doesn't allow a phsp at 50 mm when using phantom
        #filenamebeam    = filename + '_phsp_' + beam

        #print '--------------------------------------'
        #print 'This is ' + filenamebeam + ' (phsp)'
        #print '--------------------------------------'

        ### delete existing files
        #resp = requests.get(url+'/delete/'+filenamebeam + '_field.phsp')
        #resp = requests.get(url+'/delete/'+filenamebeam + '_field.header')

        #params['filename']     = filenamebeam
        #params['phantom_bool'] = None
        #params['phsp_record']  = 'True'
        #params['phsp_position'] = 50

        #if precision == 0:
        #    params['incident_particles'] = 1000000

        #helper.print_params(params)

        #print 'Submitting job:'
        #submit = requests.post(url, data=params)
        #helper._wait_for_jobs(url)

        ## get output file and macro
        #ofn = filenamebeam + '.output'
        #helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        #ofn = filenamebeam + '.mac'
        #helper.get_file(url + '/vl_files/' + ofn, ofn, directory=directory)

        ## plot phase spaces
        #plt_url = url + '/figphsp/' + filenamebeam + '_field.phsp'
        #plt_params = {
        #    'particle'  : 2,
        #    'xaxis'     : 'x',
        #    'fluence'   : 'True',
        #    'xmin'      : '-30.0',
        #    'xmax'      : 30.0,
        #    'xnumber_bins' : 120,
        #    'condition' : '(u**2 + v**2 < 0.9999) and abs(y)<2',
        #    'compare'   : 'True',
        #    'msmt_file' : prf_air_msmt
        #}

        #helper.get_file(plt_url, filenamebeam + '.x.png',
        #        params=plt_params, directory=directory, timeout=1800)
