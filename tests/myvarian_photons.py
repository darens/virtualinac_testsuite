"""
   VirtuaLinac.myvarian_photons
   ~~~~~~~~~~~~~~~~~~~~~~~

   Calculate dose distributions starting from photon phase space files

   :copyright: (c) 2016 Varian Medical Systems
   :license: MIT
   :author: Daren Sawkey (daren dot sawkey at varian dot com)
"""

# myvarian_photons.py
#
# Copyright (c) 2016 Varian Medical Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os,sys
import time
import requests
import shutil
import vl_api_helper as helper
import myvarian_photons_config

def myvarian_photons(url,
                     precision=0,
                     code_version='10.2.p1',
                     upload=True,
                     phsp_index=0,
                     filename='myvarian_photons',
                     beams=None
                     ):
    """ Calculate dose to water for photon beams, starting from phsp """
    ### starting from myvarian phase space file

    if beams is None:
        beams = ['4X', '6X', '6FFF', '8X', '10X', '10FFF', '15X']
    
    config = myvarian_photons_config.config(phsp_index)
    params = myvarian_photons_config.params()
            
    directory = filename
    #if os.path.exists(directory):
    #    directory = directory + '_' + str(int(time.time()))
    if not os.path.isdir(directory):
        os.mkdir(directory)

    for beam in beams:

        source_phsp = config['source_phsp'][beam]
        filenamebeam    = filename + '_' + beam
        data_dir = os.path.join(config['data_dir'], beam)
        phsp_dir = os.path.join(config['phsp_dir'], beam)
        pdd_msmt = config['pdd'][beam]
        prf_msmt = config['prf'][beam]

        print '--------------------------------------'
        print 'This is ' + filenamebeam
        print '--------------------------------------'
       
       ## delete existing files
        resp = requests.get(url+'/delete/'+filenamebeam + '.dose')

        if upload:
            #upload measurement files
            print 'Uploading measurement files.'
            upload_url = url + '/upload'
            files = {'file': open(os.path.join(data_dir, pdd_msmt), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(data_dir, prf_msmt), 'rb')}
            r = requests.post(upload_url, files=files)

            ## upload phsp file
            print 'Uploading phase space files.'
            files = {'file': open(os.path.join(phsp_dir, source_phsp), 'rb')}
            r = requests.post(upload_url, files=files)
            files = {'file': open(os.path.join(
                    phsp_dir, source_phsp.split('.')[0] + '.IAEAheader'), 'rb')}
            r = requests.post(upload_url, files=files)

        #print options
        params['filename']     = filenamebeam
        params['phsp_input_file'] = source_phsp
        if precision == 0:
            params['incident_particles'] = 1000000

        helper.print_params(params)

        print 'Submitting job:'
        submit = requests.post(url, data=params)
        helper._wait_for_jobs(url)

        average = 2
        if precision == 0:
            average = 5

        # plot dose distributions
        plt_url = url + '/fig/' + filenamebeam + '.dose'
        plt_params = {
            'direction' : 'z',
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : pdd_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.Z.png', params=plt_params,
                directory=directory)

        plt_params = {
            'direction' : 'x',
            'voxel_z'   : config['voxel_z'][beam],
            'average'   : average,
            'compare'   : 'True',
            'msmt_file' : prf_msmt
        }
        helper.get_file(plt_url, filenamebeam + '.X.png', params=plt_params, 
                directory=directory)

